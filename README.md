![Build Status](https://gitlab.com/efnet-moto/efnetmoto.gitlab.io/badges/master/build.svg)

----

EFNet #motorcycles [Pelican] website using GitLab Pages.

---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:2.7-alpine

pages:
  script:
  - pip install -r requirements.txt
  - pelican -s publishconf.py
  artifacts:
    paths:
    - public/
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Pelican
1. Generate the website: `make html`
1. Preview your project: `make serve`
1. Add content

Read more at Pelican's [documentation].

[ci]: https://about.gitlab.com/gitlab-ci/
[pelican]: http://blog.getpelican.com/
[install]: http://docs.getpelican.com/en/3.6.3/install.html
[documentation]: http://docs.getpelican.com/
