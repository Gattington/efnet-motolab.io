Title: New website on GitLab Pages
Date: 2017-07-16

The site is now on GitLab Pages.  You can contribute by submitting a merge request at <https://gitlab.com/efnet-moto/efnet-moto.gitlab.io>.

05-06-2021:
    Enrollment for EFnet #motorcycles 2023 Dragonweek is now open.
    Reservation is from Noon April 29th 2023, to Noon May 6th, 2023.
    Price is $150 for the week @ Treetops. 
    9 available slots, if we surpass 9 we will reserve The Point.
    Partial Rates available for Partial Stays, contact mist0p for arrangements.
    holla@gattington.com for more info.
    Paypal payments to holla@gattington.com.

        ~mist0p
